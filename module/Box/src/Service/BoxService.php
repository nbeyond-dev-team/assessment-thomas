<?php
/**
 * &Beyond Sales Engineers (http://www.nbeyond.nl).
 *
 * @copyright Copyright (c) 2017 &Beyond Sales Engineers (http://www.nbeyond.nl)
 */

namespace Box\Service;

class BoxService
{
    protected $dependency;

    public function __construct($dependency)
    {
        $this->dependency = $dependency;
    }
   

    public function writeLog()
    {
        // Store an event-log to the ./data/log/events.log file 
        // Please note that the applications Current-Working-Directory has been set to the application-root folder
    }
}
